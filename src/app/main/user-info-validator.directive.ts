import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";

@Directive({
  selector: '[appUserInfoValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting: UserInfoValidatorDirective, multi: true}]

})
export class UserInfoValidatorDirective implements Validator {
  @Input()
  appUserInfoValidator = '';

  validate(control: AbstractControl): ValidationErrors | null {
    if (control.value && control.value.includes(this.appUserInfoValidator)) {
      return null;
    } else {
      return {appTemplateValidator: this.appUserInfoValidator};
    }
  }

  registerOnValidatorChange(fn: () => void): void {
  }


}
