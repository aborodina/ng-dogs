import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Dog} from "../../shared/dogs.model";
import {DogsService} from "../../shared/dogs.service";
import {ActivatedRoute, Router} from "@angular/router";
import {finalize} from "rxjs/operators";

@Component({
  selector: 'app-edit-ad',
  templateUrl: './edit-ad.component.html',
  styleUrls: ['./edit-ad.component.scss']
})
export class EditAdComponent implements OnInit {

  form: FormGroup;
  loading = false;

  constructor(private dogsService: DogsService,
              private route: ActivatedRoute,
              private router: Router,) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl({value: '', disabled: true}, Validators.required),
      breed: new FormControl(null, Validators.required),
      price: new FormControl(null, Validators.required),
      info: new FormControl(null),
      id: new FormControl(null)
    });
    const id = this.dogsService;
    this.route.params.subscribe((param: { id: string }) => {
      this.loading = true;
      this.dogsService.getDogsById(+param.id).subscribe((dog) => {
        this.form.patchValue(dog);
        this.loading = false;
      })
    })
  }

  onSave() {
    const {name, breed, price, info, id} = this.form.getRawValue();
    const dog = new Dog(name, breed, price, info, id);

    this.loading = true;
    this.dogsService.editDog(dog)
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => {
      this.router.navigate(['/dogs'])
    })
  }

}
