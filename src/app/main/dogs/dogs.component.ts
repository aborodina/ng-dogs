import {Component, OnInit} from '@angular/core';
import {Dog} from "../../shared/dogs.model";
import {DogsService} from "../../shared/dogs.service";
import {FormControl} from "@angular/forms";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";

interface SortedVariant {
  field: string,
  title: string,
  reverse: boolean
}

@Component({
  selector: 'app-dogs',
  templateUrl: './dogs.component.html',
  styleUrls: ['./dogs.component.scss']
})
export class DogsComponent implements OnInit {

  dogs: Dog[];
  dogsView: Dog[];
  loading = false;
  nameFilter = new FormControl();
  sort = new FormControl();
  breedFilter = new FormControl();
  priceFilter = new FormControl();

  sortedVariants: SortedVariant[] = [
    {
      field: 'name',
      title: 'По имени ↑',
      reverse: true
    }, {
      field: 'name',
      title: 'По имени  ↓',
      reverse: false
    },
    {
      field: 'price',
      title: 'По цене ↑',
      reverse: true
    },
    {
      field: 'price',
      title: "По цене ↓",
      reverse: false
    }
  ];

  constructor(private dogsService: DogsService) {
  }

  ngOnInit() {
    this.loading = true;
    this.dogsService.getDogs().subscribe((dogs) => {
      this.dogs = dogs;
      this.dogsView = dogs;
      this.loading = false;
    });

    this.nameFilter.valueChanges
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe((value) => this.filterName(value));

    this.breedFilter.valueChanges
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe((value) => this.filterBreed(value));

    this.priceFilter.valueChanges
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe((value) => this.filterPrice(value));


    this.sort.valueChanges.subscribe((value: SortedVariant) => this.dogsSort(value))
  }

  delete(dog) {
    this.loading = true;
    this.dogsService.deleteDog(dog).subscribe((value) => {
      this.dogs = this.dogs.filter((item) => item.id !== dog.id);
      this.loading = false
    });
  }

  filterName(value) {
    if (value) {
      this.dogsView = this.dogs.filter((item) => item.name === value);
    } else {
      this.dogsView = this.dogs
    }
  }

  filterBreed(value) {
    if (value) {
      this.dogsView = this.dogs.filter((item) => item.breed === value);
    } else {
      this.dogsView = this.dogs
    }
  }

  filterPrice(value) {
    if (value) {
      this.dogsView = this.dogs.filter((item) => item.price === value);
    } else {
      this.dogsView = this.dogs
    }
  }

  dogsSort(value: SortedVariant) {
    this.dogsView = this.dogsView.sort((a, b) => {
      if (a[value.field] > b[value.field]) {
        return 1;
      }
      if (a[value.field] < b[value.field]) {
        return -1;
      }
      return 0;
    });
    if(value.reverse){
      this.dogsView =  this.dogsView.reverse();
    }
  }
}
