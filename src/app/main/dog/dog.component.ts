import {Component, OnInit} from '@angular/core';
import {Dog} from "../../shared/dogs.model";
import {DogsService} from "../../shared/dogs.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-dog',
  templateUrl: './dog.component.html',
  styleUrls: ['./dog.component.scss']
})
export class DogComponent implements OnInit {

  dog: Dog;
  loading = false;

  constructor(private dogsService: DogsService,
              private route: ActivatedRoute) {
    this.route.params.subscribe((param: { id: string }) => {
      this.loading = true;
      this.dogsService.getDogsById(+param.id).subscribe((dog) => {
        this.dog = dog;
        this.loading = false;
      })
    })
  }

  ngOnInit() {
  }

}
