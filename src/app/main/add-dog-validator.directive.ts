import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {myFunction} from "../shared/my-validate.function";

@Directive({
  selector: '[appAddDogValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting: AddDogValidatorDirective, multi: true}]
})
export class AddDogValidatorDirective implements Validator {
  @Input()
  appDogValidator = 'Мопс';

  validate(control: AbstractControl): ValidationErrors | null {
      if (control.value && control.value.includes(this.appDogValidator)) {
        return null;
      } else {
        return {appDogValidator: this.appDogValidator};
      }
  }

  registerOnValidatorChange(fn: () => void): void {
  }
}
