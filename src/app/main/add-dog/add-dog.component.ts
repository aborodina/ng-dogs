import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {Dog} from "../../shared/dogs.model";
import {Router} from "@angular/router";
import {DogsService} from "../../shared/dogs.service";
import {myFunction} from "../../shared/my-validate.function";

@Component({
  selector: 'app-add-dog',
  templateUrl: './add-dog.component.html',
  styleUrls: ['./add-dog.component.scss']
})
export class AddDogComponent implements OnInit {

  form: FormGroup;
  breedList = ['Мопс', 'Бульдог', 'Такса', 'Овчарка'];
  loading = false;
  priceList = ['Бесплатно', '100', '5000', 'По дговоренности'];

  constructor(
    private router: Router,
    private dogsService: DogsService
  ) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      breed: new FormControl(null, [Validators.required, this.breedFilterWithParam(this.breedList)]),
      price: new FormControl(null, [Validators.required, this.priceValidatorWithParam(this.priceList)]),
      info: new FormControl(null)
    })
  }

  onSubmit() {
    const {name, breed, price, info} = this.form.value;
    const dog = new Dog(name, breed, price, info);

    this.loading = true;
    this.dogsService.createNewDog(dog)
      .subscribe(() => {
        this.router.navigate(['/dogs']);
        this.loading = false;
      })
  }

  breedFilter(control: AbstractControl): ValidationErrors | null {
    if (control.value && control.value.includes('Мопс' || 'Бульдог' || 'Такса' || 'Овчарка')) {
      return null;
    } else {
      return {appTemplateValidator: 'Породы нет'};
    }
  }

  breedFilterWithParam(breedList: string[]): (control: AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      if (control.value && control.value.includes(breedList)) {
        return null;
      } else {
        return {appTemplateValidator: 'Такой породы нет'};
      }
    }
  }

  priceValidatorWithParam(priceList: string[]): (control: AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      if (control.value && control.value.includes(priceList)) {
        return null;
      } else {
        return {appTemplateValidator: 'Укажите в строке "100", "5000" или "По договоренности"'};
      }
    }
  }
}
