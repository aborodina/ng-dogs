import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {BaseApi} from "../../shared/base-api";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class FileUploadService extends BaseApi {
  constructor(public http: HttpClient) {
    super(http)
  }

  postFile(fileToUpload: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('fileKey', fileToUpload, fileToUpload.name);
    return this.post('picture', formData)
  }
}
