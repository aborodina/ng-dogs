import {NgModule} from "@angular/core";
import {MainRoutingModule} from "./main-routing.module";
import {AddDogComponent} from "./add-dog/add-dog.component";
import {MainComponent} from "./main.component";
import {HomeComponent} from "./home/home.component";
import {DogsComponent} from "./dogs/dogs.component";
import {HeaderComponent} from "./shared/components/header/header.component";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import { BreedsComponent } from './breeds/breeds.component';
import { EditAdComponent } from './edit-ad/edit-ad.component';
import { DogComponent } from './dog/dog.component';
import { AddDogValidatorDirective } from './add-dog-validator.directive';
import { UserInfoComponent } from './user-info/user-info.component';
import { UserInfoValidatorDirective } from './user-info-validator.directive';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from "@angular/material/card";
import {MatMenuModule} from "@angular/material/menu";

@NgModule({
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatMenuModule
  ],
  declarations: [
    MainComponent,
    AddDogComponent,
    HomeComponent,
    DogsComponent,
    HeaderComponent,
    BreedsComponent,
    EditAdComponent,
    DogComponent,
    AddDogValidatorDirective,
    UserInfoComponent,
    UserInfoValidatorDirective,
  ]
})

export class MainModule {

}
