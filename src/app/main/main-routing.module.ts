import {RouterModule, Routes} from "@angular/router";
import {MainComponent} from "./main.component";
import {HomeComponent} from "./home/home.component";
import {DogsComponent} from "./dogs/dogs.component";
import {NgModule} from "@angular/core";
import {AddDogComponent} from "./add-dog/add-dog.component";
import {BreedsComponent} from "./breeds/breeds.component";
import {EditAdComponent} from "./edit-ad/edit-ad.component";
import {DogComponent} from "./dog/dog.component";
import {UserInfoComponent} from "./user-info/user-info.component";

const routers: Routes = [
  {
    path: '', component: MainComponent, children: [
      {path: 'home', component: HomeComponent},
      {path: 'breeds', component: BreedsComponent},
      {path: 'dogs', component: DogsComponent},
      {path: 'dogs/:id', component: DogComponent},
      {path: 'dogs/:id/edit', component: EditAdComponent},
      {path: 'add-dog', component: AddDogComponent},
      {path: 'user-info', component: UserInfoComponent}
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routers)],
  exports: [RouterModule]
})

export class MainRoutingModule {
}
