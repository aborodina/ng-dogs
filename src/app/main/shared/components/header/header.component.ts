import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {User} from "../../../../shared/user.model";
import {AuthService} from "../../../../shared/auth.service";
import {Router} from "@angular/router";
import {fromEvent} from "rxjs";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
  @ViewChild('menu', {static: true})
  menu: ElementRef;

  showFullMenu: boolean = true;

  user: User;
  date: Date = new Date();

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.user = JSON.parse(window.localStorage.getItem('user'));

  }

  ngAfterViewInit(): void {
    fromEvent(window, 'resize').subscribe((value) => {
      console.log(value);
      this.showFullMenu = (value.target as any).innerWidth > 700
    })
  }

  onLogout() {
    this.authService.logout();
    this.router.navigate(['/login'])
  }
}
