import {Injectable} from "@angular/core";
import {BaseApi} from "./base-api";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "./user.model";
import {delay, map} from "rxjs/operators";

@Injectable()

export class UsersService extends BaseApi {

  constructor(public http: HttpClient) {
    super(http);
  }

  getUserByEmail(email: string): Observable<User> {
    return this.get(`users?email=${email}`).pipe(
      map<User[], User>((user: User[]) => user[0] ? user[0] : undefined),
      delay(2000))

  }

  createNewUser(user: User): Observable<User> {
    return this.post('users', user);
  }
}
