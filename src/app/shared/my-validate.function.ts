import {AbstractControl, ValidationErrors} from "@angular/forms";

export function myFunction(control: AbstractControl): ValidationErrors | null {
  if (control.value && control.value.includes(this.appDogValidator)) {
    return null;
  } else {
    return {appDogValidator: this.appDogValidator};
  }
}
