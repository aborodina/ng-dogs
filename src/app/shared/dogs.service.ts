import {Injectable} from "@angular/core";
import {BaseApi} from "./base-api";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Dog} from "./dogs.model";
import {delay, map} from "rxjs/operators";

@Injectable()

export class DogsService extends BaseApi {
  constructor(public http: HttpClient) {
    super(http);
  }

  getDogsById(id: number): Observable<Dog> {
    return this.get(`dogs?id=${id}`).pipe(
      map<Dog[], Dog>((dog: Dog[]) => dog[0] ? dog[0] : undefined),
      delay(2000));
  }


  getDogs(): Observable<Dog[]> {
    return this.get(`dogs`).pipe(
    delay(2000))
  }

  createNewDog(dog: Dog): Observable<Dog> {
    return this.post('dogs', dog).pipe(
      delay(2000))
  }

  editDog(dog: Dog): Observable<Dog> {
    return this.put('dogs/' + dog.id, dog).pipe(
      delay(2000))
  }

  deleteDog(dog: Dog): Observable<any> {
    return this.delete('dogs/' + dog.id).pipe(
      delay(2000))
  }
}
