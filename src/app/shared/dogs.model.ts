export class Dog {
  constructor(
    public name: string,
    public breed: string,
    public price: number,
    public info?: string,
    public id?: number
  ) {}
}
