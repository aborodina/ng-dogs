import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {AuthModule} from "./auth/auth.module";
import {AppRoutingModule} from "./app-routing.module";
import {MainModule} from "./main/main.module";
import {UsersService} from "./shared/users.service";
import {AuthService} from "./shared/auth.service";
import {DogsService} from "./shared/dogs.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AuthModule,
    AppRoutingModule,
    MainModule,
    BrowserAnimationsModule
  ],
  providers: [UsersService, AuthService, DogsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
