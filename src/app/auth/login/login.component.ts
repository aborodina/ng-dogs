import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UsersService} from "../../shared/users.service";
import {User} from "../../shared/user.model";
import {Message} from "../../shared/message.model";
import {AuthService} from "../../shared/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  message: Message = new Message('danger', '');
  loading = false;

  constructor(
    private router: Router,
    private userService: UsersService,
    private authService: AuthService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)])
    })
  }

  private showMessage(message: Message) {
    debugger
    this.message = message;
    window.setTimeout(() => {
      this.message.text = '';
    }, 3000);
  }

  onSubmit() {
    const formData = this.form.value;
    this.loading = true;
    this.userService.getUserByEmail(formData.email).subscribe((user: User) => {

      if (user) {
        if (user.password === formData.password) {
          this.message.text = '';
          window.localStorage.setItem('user', JSON.stringify(user));
          this.authService.login();
          this.loading = false;
          this.router.navigate(['/main', 'home'])
        } else {
          this.showMessage({text: 'Пароль неверный', type: 'danger'});
        }
      } else {
        this.showMessage({text: 'Такого пользователя не существует', type: 'danger'})
      }
    })
  }
}
